/*
 * Copyright 1995-2021 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the OpenSSL license (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <openssl/opensslconf.h>

#ifndef OPENSSL_NO_SOCK

#include "apps.h"
#include "progs.h"
#include <openssl/x509.h>
#include <openssl/ssl.h>
#include <openssl/pem.h>
#include "s_apps.h"
#include <openssl/err.h>
#include <internal/sockets.h>

#if !defined(OPENSSL_SYS_MSDOS)

# include OPENSSL_UNISTD

#endif

#define SSL_CONNECT_NAME        "localhost:4433"

#define SECONDS 30
#define SECONDSSTR "30"
static int called = 0;

static SSL *doConnection(SSL *scon, const char *host, SSL_CTX *ctx, SSL_SESSION *sess, int mpsk);

/*
 * Define a HTTP get command globally.
 * Also define the size of the command, this is two bytes less than
 * the size of the string because the %s is replaced by the URL.
 */
static const char fmt_http_get_cmd[] = "GET %s HTTP/1.0\r\n\r\n";
static const size_t fmt_http_get_cmd_size = sizeof(fmt_http_get_cmd) - 2;

typedef enum OPTION_choice {
    OPT_ERR = -1, OPT_EOF = 0, OPT_HELP,
    OPT_CONNECT, OPT_CIPHER, OPT_CIPHERSUITES, OPT_CERT, OPT_NAMEOPT, OPT_KEY,
    OPT_CAPATH, OPT_CAFILE, OPT_NOCAPATH, OPT_NOCAFILE, OPT_NEW, OPT_REUSE,
    OPT_BUGS, OPT_VERIFY, OPT_TIME, OPT_SSL3,
    OPT_WWW, OPT_PSK
} OPTION_CHOICE;

const OPTIONS s_time_options[] = {
        {"help", OPT_HELP, '-', "Display this summary"},
        {"connect", OPT_CONNECT, 's',
         "Where to connect as post:port (default is " SSL_CONNECT_NAME ")"},
        {"cipher", OPT_CIPHER, 's', "TLSv1.2 and below cipher list to be used"},
        {"ciphersuites", OPT_CIPHERSUITES, 's',
         "Specify TLSv1.3 ciphersuites to be used"},
        {"cert", OPT_CERT, '<', "Cert file to use, PEM format assumed"},
        {"nameopt", OPT_NAMEOPT, 's', "Various certificate name options"},
        {"key", OPT_KEY, '<', "File with key, PEM; default is -cert file"},
        {"CApath", OPT_CAPATH, '/', "PEM format directory of CA's"},
        {"cafile", OPT_CAFILE, '<', "PEM format file of CA's"},
        {"CAfile", OPT_CAFILE, '<', "PEM format file of CA's"},
        {"no-CAfile", OPT_NOCAFILE, '-',
         "Do not load the default certificates file"},
        {"no-CApath", OPT_NOCAPATH, '-',
         "Do not load certificates from the default certificates directory"},
        {"new", OPT_NEW, '-', "Just time new connections"},
        {"reuse", OPT_REUSE, '-', "Just time connection reuse"},
        {"bugs", OPT_BUGS, '-', "Turn on SSL bug compatibility"},
        {"verify", OPT_VERIFY, 'p',
         "Turn on peer certificate verification, set depth"},
        {"time", OPT_TIME, 'p', "Seconds to collect data, default " SECONDSSTR},
        {"www", OPT_WWW, 's', "Fetch specified page from the site"},
        {"psk", OPT_PSK, '-', "Use a psk, will break if server/cypher doesn't support early data"},
#ifndef OPENSSL_NO_SSL3
        {"ssl3", OPT_SSL3, '-', "Just use SSLv3"},
#endif
        {NULL}
};

#define START   0
#define STOP    1

static double tm_Time_F(int s) {
    return app_tminterval(s, 1);
}

int s_time_main(int argc, char **argv) {
    char buf[1024 * 8];
    SSL *scon = NULL;
    SSL_CTX *ctx = NULL;
    const SSL_METHOD *meth = NULL;
    char *CApath = NULL, *CAfile = NULL, *cipher = NULL, *ciphersuites = NULL;
    char *www_path = NULL;
    char *host = SSL_CONNECT_NAME, *certfile = NULL, *keyfile = NULL, *prog;
    double totalTime = 0.0;
    int noCApath = 0, noCAfile = 0;
    int maxtime = SECONDS, nConn = 0, perform = 3, ret = 1, i, st_bugs = 0;
    long bytes_read = 0, finishtime = 0;
    OPTION_CHOICE o;
    int max_version = 0, ver, buf_len;
    size_t buf_size;
    int psk = 0;
    SSL_SESSION *sess = NULL;
    meth = TLS_client_method();

    prog = opt_init(argc, argv, s_time_options);
    while ((o = opt_next()) != OPT_EOF) {
        switch (o) {
            case OPT_EOF:
            case OPT_ERR:
            opthelp:
                BIO_printf(bio_err, "%s: Use -help for summary.\n", prog);
                goto end;
            case OPT_HELP:
                opt_help(s_time_options);
                ret = 0;
                goto end;
            case OPT_CONNECT:
                host = opt_arg();
                break;
            case OPT_REUSE:
                perform = 2;
                break;
            case OPT_NEW:
                perform = 1;
                break;
            case OPT_VERIFY:
                if (!opt_int(opt_arg(), &verify_args.depth))
                    goto opthelp;
                BIO_printf(bio_err, "%s: verify depth is %d\n",
                           prog, verify_args.depth);
                break;
            case OPT_CERT:
                certfile = opt_arg();
                break;
            case OPT_NAMEOPT:
                if (!set_nameopt(opt_arg()))
                    goto end;
                break;
            case OPT_KEY:
                keyfile = opt_arg();
                break;
            case OPT_CAPATH:
                CApath = opt_arg();
                break;
            case OPT_CAFILE:
                CAfile = opt_arg();
                break;
            case OPT_NOCAPATH:
                noCApath = 1;
                break;
            case OPT_NOCAFILE:
                noCAfile = 1;
                break;
            case OPT_CIPHER:
                cipher = opt_arg();
                break;
            case OPT_CIPHERSUITES:
                ciphersuites = opt_arg();
                break;
            case OPT_BUGS:
                st_bugs = 1;
                break;
            case OPT_TIME:
                if (!opt_int(opt_arg(), &maxtime))
                    goto opthelp;
                break;
            case OPT_WWW:
                www_path = opt_arg();
                buf_size = strlen(www_path) + fmt_http_get_cmd_size;
                if (buf_size > sizeof(buf)) {
                    BIO_printf(bio_err, "%s: -www option is too long\n", prog);
                    goto end;
                }
                break;
            case OPT_PSK:
                psk = 1;
                break;
            case OPT_SSL3:
                max_version = SSL3_VERSION;
                break;
        }
    }
    argc = opt_num_rest();
    if (argc != 0)
        goto opthelp;

    if (cipher == NULL)
        cipher = getenv("SSL_CIPHER");

    if ((ctx = SSL_CTX_new(meth)) == NULL)
        goto end;

    SSL_CTX_set_mode(ctx, SSL_MODE_AUTO_RETRY);
    SSL_CTX_set_quiet_shutdown(ctx, 1);
    if (SSL_CTX_set_max_proto_version(ctx, max_version) == 0)
        goto end;

    if (st_bugs)
        SSL_CTX_set_options(ctx, SSL_OP_ALL);
    if (cipher != NULL && !SSL_CTX_set_cipher_list(ctx, cipher))
        goto end;
    if (ciphersuites != NULL && !SSL_CTX_set_ciphersuites(ctx, ciphersuites))
        goto end;
    if (!set_cert_stuff(ctx, certfile, keyfile))
        goto end;

    if (!ctx_set_verify_locations(ctx, CAfile, CApath, noCAfile, noCApath)) {
        ERR_print_errors(bio_err);
        goto end;
    }
    if (!(perform & 1))
        goto next;
    printf("Collecting connection statistics for %d seconds\n", maxtime);

    /* Loop and time how long it takes to make connections */

    bytes_read = 0;
    finishtime = (long) time(NULL) + maxtime;
    tm_Time_F(START);
    for (;;) {
        if (finishtime < (long) time(NULL))
            break;

        if ((scon = doConnection(NULL, host, ctx, NULL, 0)) == NULL)
            goto end;

        if (www_path != NULL) {
            buf_len = BIO_snprintf(buf, sizeof(buf), fmt_http_get_cmd,
                                   www_path);
            if (buf_len <= 0 || SSL_write(scon, buf, buf_len) <= 0)
                goto end;
            while ((i = SSL_read(scon, buf, sizeof(buf))) > 0)
                bytes_read += i;
        }
        SSL_set_shutdown(scon, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
        BIO_closesocket(SSL_get_fd(scon));

        nConn += 1;
        if (SSL_session_reused(scon)) {
            ver = 'r';
        } else {
            ver = SSL_version(scon);
            if (ver == TLS1_VERSION)
                ver = 't';
            else if (ver == SSL3_VERSION)
                ver = '3';
            else
                ver = '*';
        }
        fputc(ver, stdout);
        fflush(stdout);

        SSL_free(scon);
        scon = NULL;
    }
    totalTime += tm_Time_F(STOP); /* Add the time for this iteration */

    i = (int) ((long) time(NULL) - finishtime + maxtime);
    printf
            ("\n\n%d connections in %.2fs; %.2f connections/user sec, bytes read %ld\n",
             nConn, totalTime, ((double) nConn / totalTime), bytes_read);
    printf
            ("%d connections in %ld real seconds, %ld bytes read per connection\n",
             nConn, (long) time(NULL) - finishtime + maxtime,
             nConn > 0 ? bytes_read / nConn : 0l);

    /*
     * Now loop and time connections using the same session id over and over
     */

    next:
    if (!(perform & 2))
        goto end;
    printf("\n\nNow timing with session id reuse.\n");

    /* Get an SSL object so we can reuse the session id */
    if ((scon = doConnection(NULL, host, ctx, NULL, 0)) == NULL) {
        BIO_printf(bio_err, "Unable to get connection\n");
        goto end;
    }

    if (www_path != NULL) {
        buf_len = BIO_snprintf(buf, sizeof(buf), fmt_http_get_cmd, www_path);
        if (buf_len <= 0 || SSL_write(scon, buf, buf_len) <= 0)
            goto end;
        while ((i = SSL_read(scon, buf, sizeof(buf))) > 0)
            continue;
    }
    SSL_set_shutdown(scon, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
    BIO_closesocket(SSL_get_fd(scon));

    nConn = 0;
    totalTime = 0.0;

    finishtime = (long) time(NULL) + maxtime;

    printf("starting\n");
    bytes_read = 0;
    tm_Time_F(START);

    for (;;) {
        if (finishtime < (long) time(NULL))
            break;

        if (doConnection(scon, host, ctx, NULL, 0) == NULL)
            goto end;

        if (www_path != NULL) {
            buf_len = BIO_snprintf(buf, sizeof(buf), fmt_http_get_cmd,
                                   www_path);
            if (buf_len <= 0 || SSL_write(scon, buf, buf_len) <= 0)
                goto end;
            while ((i = SSL_read(scon, buf, sizeof(buf))) > 0)
                bytes_read += i;
        }
        SSL_set_shutdown(scon, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
        BIO_closesocket(SSL_get_fd(scon));

        nConn += 1;
        if (SSL_session_reused(scon)) {
            ver = 'r';
        } else {
            ver = SSL_version(scon);
            if (ver == TLS1_VERSION)
                ver = 't';
            else if (ver == SSL3_VERSION)
                ver = '3';
            else
                ver = '*';
        }
        fputc(ver, stdout);
        fflush(stdout);
    }
    totalTime += tm_Time_F(STOP); /* Add the time for this iteration */

    printf
            ("\n\n%d connections in %.2fs; %.2f connections/user sec, bytes read %ld\n",
             nConn, totalTime, ((double) nConn / totalTime), bytes_read);
    printf
            ("%d connections in %ld real seconds, %ld bytes read per connection\n",
             nConn, (long) time(NULL) - finishtime + maxtime, bytes_read / nConn);
    if (psk) {
        printf("\n\nNow timing with 0-RTT.\n");

        /* Get an SSL object so we can reuse the session */
        if ((scon = doConnection(NULL, host, ctx, NULL, 1)) == NULL) {
            BIO_printf(bio_err, "Unable to get connection\n");
            goto end;
        }
        int forky = fork();
        if (forky == 0) {
            SSL_write(scon, "hi", 2);
            SSL_read(scon, NULL, 0);
            printf("I'm dead\n");
            exit(1);
        } else {
            sleep(1);
            kill(forky, SIGINT);
        }
        BIO *stmp = BIO_new_file("newsession", "r");
        if (stmp == NULL) {
            BIO_printf(bio_err, "Can't open session file %s\n", "newsession");
            ERR_print_errors(bio_err);
            goto end;
        }
        sess = PEM_read_bio_SSL_SESSION(stmp, NULL, 0, NULL);
        BIO_free(stmp);
        if (sess == NULL) {
            BIO_printf(bio_err, "Can't open session file %s\n", "newsession");
            ERR_print_errors(bio_err);
            goto end;
        }
        if (www_path != NULL) {
            buf_len = BIO_snprintf(buf, sizeof(buf), fmt_http_get_cmd, www_path);
            if (buf_len <= 0 || SSL_write(scon, buf, buf_len) <= 0)
                goto end;
            while ((i = SSL_read(scon, buf, sizeof(buf))) > 0)
                continue;
        }
        SSL_set_shutdown(scon, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
        BIO_closesocket(SSL_get_fd(scon));

        nConn = 0;
        totalTime = 0.0;
        finishtime = (long) time(NULL) + maxtime;

        printf("starting\n");
        bytes_read = 0;
        tm_Time_F(START);

        for (;;) {
            if (finishtime < (long) time(NULL))
                break;

            if ((scon = doConnection(NULL, host, ctx, sess, 0)) == NULL)
                goto end;

            SSL_read(scon, NULL, 0);

            BIO *stmp = BIO_new_file("newsession", "r");
            if (stmp == NULL) {
                BIO_printf(bio_err, "Can't open session file %s\n", "newsession");
                ERR_print_errors(bio_err);
                goto end;
            }
            sess = PEM_read_bio_SSL_SESSION(stmp, NULL, 0, NULL);
            BIO_free(stmp);
            if (sess == NULL) {
                BIO_printf(bio_err, "Can't open session file %s\n", "newsession");
                ERR_print_errors(bio_err);
                goto end;
            }
            SSL_set_shutdown(scon, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
            BIO_closesocket(SSL_get_fd(scon));

            nConn += 1;
            if (SSL_session_reused(scon)) {
                ver = 'r';
            } else {
                ver = SSL_version(scon);
                if (ver == TLS1_VERSION)
                    ver = 't';
                else if (ver == SSL3_VERSION)
                    ver = '3';
                else
                    ver = '*';
            }
            fputc(ver, stdout);
            fflush(stdout);
        }
        totalTime += tm_Time_F(STOP); /* Add the time for this iteration */

        printf
                ("\n\n%d connections in %.2fs; %.2f connections/user sec, bytes read %ld\n",
                 nConn, totalTime, ((double) nConn / totalTime), bytes_read);
        printf
                ("%d connections in %ld real seconds, %ld bytes read per connection\n",
                 nConn, (long) time(NULL) - finishtime + maxtime, bytes_read / nConn);

    }
    ret = 0;

    end:
    SSL_free(scon);
    SSL_CTX_free(ctx);
    return ret;
}

static int new_session_cb(SSL *s, SSL_SESSION *sess) {
    called = 1;
    if (SSL_version(s) == TLS1_3_VERSION) {
        BIO *stmp = BIO_new_file("newsession", "w");

        if (stmp == NULL) {
            BIO_printf(bio_err, "Error writing session file %s\n", "newsession");
        } else {
            PEM_write_bio_SSL_SESSION(stmp, sess);
            BIO_free(stmp);
            //SSL_SESSION_print(bio_err, sess);
        }
        SSL_set_session(s, sess);
        SSL_SESSION_print(bio_err, SSL_get0_session(s));
    }
    //we only use this function for psk
    /*
     * We always return a "fail" response so that the session gets freed again
     * because we haven't used the reference.
     */
    return 0;
}

static int new_session_cb_bis(SSL *s, SSL_SESSION *sess) {
    called = 1;
    if (SSL_version(s) == TLS1_3_VERSION) {
        SSL_set_session(s, sess);
        //SSL_SESSION_print(bio_err,SSL_get0_session(s) );
    }
    //we only use this function for psk
    /*
     * We always return a "fail" response so that the session gets freed again
     * because we haven't used the reference.
     */
    return 0;
}

/*-
 * doConnection - make a connection
 */
static SSL *doConnection(SSL *scon, const char *host, SSL_CTX *ctx, SSL_SESSION *sess, int mpsk) {
    BIO *conn;
    SSL *serverCon;
    int i;

    if ((conn = BIO_new(BIO_s_connect())) == NULL)
        return NULL;

    BIO_set_conn_hostname(conn, host);
    BIO_set_conn_mode(conn, BIO_SOCK_NODELAY);
    if (sess == NULL) {

        if (scon == NULL) {
            if (mpsk) {
                SSL_CTX_set_session_cache_mode(ctx, SSL_SESS_CACHE_CLIENT
                                                    | SSL_SESS_CACHE_NO_INTERNAL);
                SSL_CTX_sess_set_new_cb(ctx, new_session_cb);
                printf("ok\n");
            }
            serverCon = SSL_new(ctx);
        } else {
            serverCon = scon;
            SSL_set_connect_state(serverCon);
        }
    } else {
        SSL_CTX_set_session_cache_mode(ctx, SSL_SESS_CACHE_CLIENT
                                            | SSL_SESS_CACHE_NO_INTERNAL);
        SSL_CTX_sess_set_new_cb(ctx, new_session_cb_bis);
        serverCon = SSL_new(ctx);
        //SSL_set_session(serverCon, sess);
        SSL_set_connect_state(serverCon);
        SSL_SESSION_print(bio_err, SSL_get0_session(serverCon));
        if (!SSL_set_session(serverCon, sess)) {
            BIO_printf(bio_err, "Can't set session\n");
            ERR_print_errors(bio_err);
            return NULL;
        }
        //SSL_SESSION_print(bio_err, SSL_get0_session(serverCon));
        if ((SSL_get0_session(serverCon) != NULL
             && SSL_SESSION_get_max_early_data(SSL_get0_session(serverCon)) > 0)) {

            SSL_set_bio(serverCon, conn, conn);
            char a = 'a';
            size_t writtenbytes;
            //SSL_SESSION_print(bio_err, SSL_get0_session(serverCon));
            if (!SSL_write_early_data(serverCon, &a, 1, &writtenbytes)) {
                switch (SSL_get_error(serverCon, 0)) {
                    case SSL_ERROR_WANT_ACCEPT:
                        BIO_printf(bio_err, "ERROR_WANT_ACCEPT\n");
                        break;
                    case SSL_ERROR_WANT_ASYNC:
                        BIO_printf(bio_err, "Error want async\n");
                        break;
                    case SSL_ERROR_WANT_READ:
                        BIO_printf(bio_err, "Error want read\n");
                        break;
                    case SSL_ERROR_SSL:
                        BIO_printf(bio_err, "error ssl\n");
                        ERR_print_errors(bio_err);
                        break;
                    case SSL_ERROR_WANT_ASYNC_JOB:
                        BIO_printf(bio_err, "Error want async job\n");
                        break;
                    case SSL_ERROR_WANT_WRITE:
                        BIO_printf(bio_err, "Error want write\n");
                        break;
                    case SSL_ERROR_WANT_CONNECT:
                        BIO_printf(bio_err, "error want connect\n");
                        break;
                    case SSL_ERROR_WANT_CLIENT_HELLO_CB:
                        BIO_printf(bio_err, "Error want hello cb\n");
                        break;
                    case SSL_ERROR_SYSCALL:
                        BIO_printf(bio_err, "Error syscall\n");
                        printf("%d", errno);
                        break;
                    default:
                        ERR_print_errors(bio_err);
                        break;
                }
                printf("early data pb\n");
                //return NULL;
            }
        } else {
            printf("Early data not supported by server\n");
            return NULL;
        }

    }

    if (sess == NULL)
        SSL_set_bio(serverCon, conn, conn
        );

/* ok, lets connect */

    i = SSL_connect(serverCon);
    if (i <= 0) {
        BIO_printf(bio_err,
                   "ERROR\n");
        if (verify_args.error != X509_V_OK)
            BIO_printf(bio_err,
                       "verify error:%s\n",
                       X509_verify_cert_error_string(verify_args
                                                             .error));
        else
            ERR_print_errors(bio_err);
        if (scon == NULL)
            SSL_free(serverCon);
        return NULL;
    }

#if defined(SOL_SOCKET) && defined(SO_LINGER)
    {
        struct linger no_linger;
        int fd;

        no_linger.l_onoff = 1;
        no_linger.l_linger = 0;
        fd = SSL_get_fd(serverCon);
        if (fd >= 0)
            (void) setsockopt(fd, SOL_SOCKET, SO_LINGER, (char *) &no_linger,
                              sizeof(no_linger));
    }
#endif

    return
            serverCon;

}

#endif /* OPENSSL_NO_SOCK */
